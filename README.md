# This is just a simple app to test openshift builds with S2I

This is a repository to play around with openshift [S2I builds](https://github.com/openshift/source-to-image). The "application" is a simple [Phoenix app](https://www.phoenixframework.org/) 
that exposes a silly, useless JSON endpoint (`/api/message`) and a few other silly ones.

# S2I in the nutshell of a nutshell

You create a “builder-image” (e.g. `mycoolapp-builder`) and you use it as a basis for building your app (`mycoolapp`).
Whenever a new build for `mycoolapp` is triggered, `mycoolapp-builder` is started in a container, the last version of your app code is injected in it and
the build is run. After the build completes, you have a new version of `mycoolapp`.

So the S2I formula is:

*coolapp_code_last_git_commit + mycoolapp_builder_image = mycoolapp_image*

# Why S2I builds?

[The S2I github page](https://github.com/openshift/source-to-image) lists some advantages. I would add:

1. If the builder image `mycoolapp-builder` is in your private docker registry, you only hit dockerhub when building the builder image.
All subsequent builds for your app image never hit dockerhub -> save time and bandwidth, as well as dockerhub usage quota
   
2. With the `--incremental` switch, previously built artifacts stored in the last version of your app image can be reused in the new
build -> save time and bandwidth as already fetched dependencies don't need to be downloaded again. Potentially already compiled code can also be reused (I haven't tried that yet)


# How to play around with the S2I app in this repository on your local machine

1. Install the S2I toolkit 
    * in case you are on a Mac: `brew install source-to-image`
2. Build the builder image: `make build`
    * This is simply a quicker way of typing: `docker build -t openshifttest-app-builder .`
    * This builds the builder image `openshifttest-app-builder`
3. Now that the builder image is available, start an S2I incremental build:
    * `s2i build --incremental ./ openshifttest-app-builder openshifttest-app`
    * Can you spot the formula? My code (`./`) + builder image (`openshifttest-app-builder`) = app image (`openshifttest-app`)
    * Now the app image is built and available
4. Run the image:
    * `docker run -ti --rm -p 4000:4000 openshifttest-app`
    * Visit `http://localhost:4000/api/message` and behold an amazing API 💪
   
Bonus points:

5. Change the code locally (**IMPORTANT: Don't forget to commit the changes!**)
6. Start another build: 
    * `s2i build --incremental ./ openshifttest-app-builder openshifttest-app`
    * Notice how dependencies are not fetched again: they are taken from the previously built image

# Test script

There is a test script for troubleshooting, which basically does all of the above and then checks if the app is up and running: `./test/run`

# How do I use S2I in my own application?

1. Create a skeleton with the S2I scripts: `s2i create <MY_APP-BUILDER> my-repo-folder`
2. Modify the `Dockerfile` and the `s2i/bin/{assemble|run|save-artifacts}` scripts so they correctly build, run, and save the
dependencies for your specific application
3. Run `./test/run` or build the builder and app images as explained above until the build works. (**Note:** If you use `./test/run` and want the final liveness
check to succeed, you will have to modify the script to adapt it to your application. However, If you are only interested in testing that the final image is correctly built, you don't
   need to modify `./test/run`)
   
# How do I use S2I in OpenShift?

1. Create a BuildConfig for your builder image that uses the Dockerfile strategy and outputs the image to: `openshifttest-app-builder:latest`
   `oc new-build <YOUR_REPO_LOCATION> --name=mycoolapp-builder`
2. If a build doesn't start automatically, trigger the build to create a builder image and wait until it's ready
3. If your application doesn't exist yet, create it with:
    * `oc new-app mycoolapp-builder:latest~<YOUR_REPO_LOCATION>`
    * If you want to use incremental builds, make sure to set `strategy.sourceStrategy.incremental=true` in your application's BuildConfig (see next point) 
4. If your application already exist, modify its BuildConfig to use S2I with your builder image:
```yaml
strategy:
  sourceStrategy:
    from:
      kind: "ImageStreamTag"
      name: "mycoolapp-builder:latest" 
    incremental: true 
```
(notice the incremental flag set to tru)

