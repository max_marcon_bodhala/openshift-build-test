# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :openshifttest, OpenshifttestWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ROKUYAPdgr/DkVJubfCw7VNbn9ZEU1TDsFtEMbYUSE7F5pKSYBg3uYRHSyXe7eZs",
  render_errors: [view: OpenshifttestWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Openshifttest.PubSub,
  live_view: [signing_salt: "Hw9W+B3G"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :openshifttest, :message, "Hello world!"
