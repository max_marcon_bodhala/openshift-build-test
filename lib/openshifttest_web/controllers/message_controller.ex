defmodule OpenshifttestWeb.MessageController do
  use OpenshifttestWeb, :controller

  def message(conn, _params) do
    render(conn, :message, %{msg: Application.get_env(:openshifttest, :message)})
  end
  
  def info(conn, _params) do 
    render(conn, :info)
  end
end
