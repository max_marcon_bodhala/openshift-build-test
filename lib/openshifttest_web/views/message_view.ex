defmodule OpenshifttestWeb.MessageView do
  use OpenshifttestWeb, :view

  def render("message.json", %{msg: msg}) do
    %{
      message: msg
    }
  end
  
  def render("info.json", _assigns)  do 
    %{
      info: "I am a test app"
    }
  end
end
